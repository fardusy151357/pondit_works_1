<x-frontend.layouts.master>

    <div class="bg-light p-5 rounded">
        <div class="row row-cols-1 row-cols-md-3 mb-3 text-center">

            @foreach ($products as $product)
            <div class="col">
                <div class="card mb-4 rounded-3 shadow-sm">
                    <div class="card-header">
                        <img src="{{ asset('storage/products/'.$product->image) }}" />
                    </div>
                    <div class="card-body">
                        <h4 class="card-title pricing-card-title">
                            <a href="{{ route('single-product', $product->id) }}"> {{ Str::limit($product->title, 50) }}</a>
                        </h4>
                        <p>{{ $product->price }} TK</p>
                        <form method="post" action="{{ route('update-cart', $product->id) }}">
                            @csrf
                            <input type="hidden" name="qty" value="1" class="form-control">
                            <button type="submit" class="btn btn-primary">Add To Card</button>
                        </form>

                    </div>
                </div>
            </div>
            @endforeach
        </div>
        {{ $products->links() }}
    </div>
</x-frontend.layouts.master>