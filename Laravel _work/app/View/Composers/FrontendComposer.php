<?php
 
namespace App\View\Composers;

use App\Models\Cart;
use App\Models\Category;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
 
class FrontendComposer
{
   
    /**
     * Bind data to the view.
     *
     * @param  \Illuminate\View\View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $totalItemsInCart = 0;
        if(Auth::check()){
            $totalItemsInCart = Cart::where('added_by', Auth::id())->get()->count();
        }
        
        $categories = Category::all();

        $view->with([
            'categories' => $categories,
            'totalItemsInCart' => $totalItemsInCart,
        ]);
    }
}